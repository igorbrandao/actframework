<!-- start: CONTENT -->
<section id="content" class="container_12 clearfix" data-sort=true>

	<?php

		// Handle multiple models
		$dashboard_model = $this->getModel();
		$user_model = $this->getModel2();

		// Check the model instance
		if ( !isset($dashboard_model) )
		{
			$dashboard_model = $this->mvc_controller->getModel();
		}

		if ( !isset($user_model) )
		{
			$user_model = $this->mvc_controller->getModel2();
		}

	?>

	<!--<ul class="stats not-on-phone">
		<li title="Usuários cadastrados">
			<strong><?php //echo $user_model->get_users_count(); ?></strong>
			<small>Usuários cadastrados</small>
		</li>
		<li title="Usuários cadastrados">
			<strong><?php //echo $user_model->get_users_count(); ?></strong>
			<small>Usuários cadastrados</small>
		</li>
		<li title="Usuários cadastrados">
			<strong><?php //echo $user_model->get_users_count(); ?></strong>
			<small>Usuários cadastrados</small>
		</li>
		<li title="Usuários cadastrados">
			<strong><?php //echo $user_model->get_users_count(); ?></strong>
			<small>Usuários cadastrados</small>
		</li>
	</ul>--><!-- End of ul.stats -->

	<h1 class="grid_12 margin-top no-margin-top-phone" title="<?php echo $this->configuration->APP_NAME; ?> - Dashboard">
		<?php echo $this->configuration->APP_NAME; ?> - Dashboard
	</h1>

	<div class="grid_12">

		<!-- start: GENERAL AVG -->
		<div class="content">
			<div class="spacer"></div>
			<div class="full-stats">
				<div class="stat hlist" data-list='[{"val":<?php echo $dashboard_model->get_accident_avg(); ?>,
				"title":"Média geral dos acidentes","color":"green"},

				{"val":<?php echo $dashboard_model->get_accident_median(); ?>,"title":"Mediana geral dos acidentes","color":"red"},

				{"val":<?php echo $dashboard_model->get_accident_mode(); ?>,"title":"Moda geral dos acidentes"}]' data-flexiwidth=true></div>
			</div>
		</div><!-- End of .content -->
		<!-- end: GENERAL AVG -->

		<?php if ( isset($this->getUser()->ID_CBO) && strcmp($this->getUser()->ID_CBO, "") != 0 ) { ?>
			<!-- start: AVG BY CBO -->
			<div class="content">
				<div class="spacer"></div>
				<div class="full-stats">
					<div class="stat hlist" data-list='[{"val":<?php echo $dashboard_model->get_accident_avg($this->getUser()->ID_CBO); ?>,
					"title":"Média de acidentes do CBO <?php echo $this->getUser()->CBO; ?>","color":"green"},

					{"val":<?php echo $dashboard_model->get_accident_median($this->getUser()->ID_CBO); ?>,
					"title":"Mediana de acidentes do CBO <?php echo $this->getUser()->CBO; ?>","color":"red"},

					{"val":<?php echo $dashboard_model->get_accident_mode( $this->getUser()->ID_CBO ); ?>,"title":"Moda de acidentes do CBO <?php echo $this->getUser()->CBO; ?>"}]' data-flexiwidth=true></div>
				</div>
			</div><!-- End of .content -->
			<!-- end: AVG BY CBO -->
		<?php } ?>

	</div>

	<!-- start: ACCIDENTS BY YEAR CHART -->
	<div class="grid_12">
		<div class="box">

			<div class="header">
				<h2><img class="icon" src="<?php echo $this->configuration->HOME_URI;?>/framework/assets/img/icons/packs/fugue/16x16/chart-up-color.png">Quantidade de acidentes totais entre 1997 - 2014</h2>

				<a href="javascript:void(0);" class="menu icon-filter" title="Filtrar informação"></a>
			</div>

			<div class="content" style="height: 300px;">
				<table class=chart >
					<thead>
						<tr>
							<?php  

								// Auxiliary variables
								$initial_year = 1997;
								$final_year = 2015;

								// Empty header
								echo "<th></th>";

								// Run the total of years
								for ( $i = $initial_year; $i < $final_year; $i++ )
								{
									echo "<th>" . $i . "</th>";
								}

							?>
						</tr>
					</thead>
					<tbody>
						<?php

							echo "<tr>";

							// Run the total of years
							for ( $i = $initial_year; $i < $final_year; $i++ )
							{
								// Open the chart line
								echo "<th>Quantidade de acidentes de trabalho por ano</th>";

								// Print the information in the line
								$count = $dashboard_model->get_qtd_accident_by_year($i);
								echo "<td alt='" . $count . "' title='" . $count . "'>" . $count . "</td>";
							}

							// Close the chart line
							echo "</tr>";

						?>

					</tbody>	
				</table>
			</div><!-- End of .content -->
		</div><!-- End of .box -->
	</div>
	<!-- end: ACCIDENTS BY YEAR CHART -->

	<!-- start: ACCIDENTS BY MONTH CHART -->
	<div class="grid_6">
		<div class="box">

			<div class="header">
				<h2><img class="icon" src="<?php echo $this->configuration->HOME_URI;?>/framework/assets/img/icons/packs/fugue/16x16/chart-up-color.png">Quantidade de acidentes por mês</h2>

				<a href="javascript:void(0);" class="menu icon-filter" title="Filtrar informação"></a>
			</div>

			<div class="content" style="height: 250px;">

				<?php

					$months = array(
					    'Janeiro',
					    'Fevereiro',
					    'Março',
					    'Abril',
					    'Maio',
					    'Junho',
					    'Julho',
					    'Agosto',
					    'Setembro',
					    'Outubro',
					    'Novembro',
					    'Dezembro'
					);

				?>

				<table class=chart data-type=bars>
					<thead>
						<tr>
							<th></th>
							<?php
								foreach ($months as $value) {
								    echo "<th>" . $value . "</th>";
								}
							?>
						</tr>
					</thead>
					<tbody>
						<?php

							echo "<tr>";

							// Run the total of years
							foreach ( $months as $value )
							{
								// Open the chart line
								echo "<th>Quantidade de acidentes de trabalho por mês</th>";

								// Print the information in the line
								$count = $dashboard_model->get_qtd_accident_by_month($value);
								echo "<td alt='" . $count . "' title='" . $count . "'>" . $count . "</td>";
							}

							// Close the chart line
							echo "</tr>";

						?>
					</tbody>	
				</table>
			</div><!-- End of .content -->
			
		</div><!-- End of .box -->
	</div><!-- End of .grid_6 -->
	<!-- end: ACCIDENTS BY MONTH CHART -->

 	<?php if ( isset($this->getUser()->ID_CBO) && strcmp($this->getUser()->ID_CBO, "") != 0 ) { ?>
		<!-- start: ACCIDENTS BY CBO AND SITUATION -->
		<div class="grid_6">
			<div class="box">
			
				<div class="header">
					<h2><img class="icon" src="<?php echo $this->configuration->HOME_URI;?>/framework/assets/img/icons/packs/fugue/16x16/chart-up-color.png">Acidentes de trabalho de <u><?php echo $this->getUser()->CBO; ?></u> por situação</h2>
	
					<a href="javascript:void(0);" class="menu icon-filter" title="Filtrar informação"></a>
				</div>
				
				<div class="content" style="height: 250px;">
					<table class=chart data-type=pie data-donut=0.6>
						<thead>
							<tr>
								<th></th>

								<?php

									// Situation list
									$situationlist = $dashboard_model->get_accident_situationlist_byCBO($this->getUser()->ID_CBO);

									foreach ( $situationlist as $value )
									{
										if ( isset( $value["DESCRICAO"] ) )
										{
											echo "<th>" . $value["DESCRICAO"] . "</th>";
										}	
									}

								?>

							</tr>
						</thead>
						<tbody>
							<?php

								// Run through the situation list
								foreach ( $situationlist as $value )
								{
									if ( isset( $value["DESCRICAO"] ) )
									{
										// Get the accidents count by situation and CBO
										$situation_count = $dashboard_model->get_accident_situationcount_byCBO($this->getUser()->ID_CBO, $value["ID_SITUACAO"]);

										echo "<tr>";
										echo "<th>" . $value["DESCRICAO"] . "</th>";
										echo "<td alt=" . $situation_count . " title=" . $situation_count . ">" . $situation_count . "</td>";
										echo "</tr>";
									}
								}

							?>
						</tbody>	
					</table>
				</div><!-- End of .content -->
				
			</div><!-- End of .box -->
		</div><!-- End of .grid_6 -->
		<!-- end: ACCIDENTS BY CBO AND SITUATION -->
	<?php } ?>

	<?php if ( isset($this->getUser()->ID_CBO) && strcmp($this->getUser()->ID_CBO, "") != 0 ) { ?>
		<!-- start: ACCIDENTS BY YEAR CHART -->
		<div class="grid_12">
			<div class="box">
	
				<div class="header">
					<h2><img class="icon" src="<?php echo $this->configuration->HOME_URI;?>/framework/assets/img/icons/packs/fugue/16x16/chart-up-color.png">Quantidade de acidentes do CBO: <u><?php echo $this->getUser()->CBO; ?></u> por ano</h2>
	
					<a href="javascript:void(0);" class="menu icon-filter" title="Filtrar informação"></a>
				</div>
	
				<div class="content" style="height: 300px;">
					<table class=chart >
						<thead>
							<tr>
								<?php  

									// Accidents count list by year and CBO
									$accidents_count_cbo = $dashboard_model->get_accident_count_byYearCBO($this->getUser()->ID_CBO);

									// Empty header
									echo "<th></th>";

									// Run through the accidents lists
									foreach ( $accidents_count_cbo as $value )
									{
										echo "<th>" . $value["ANO"] . "</th>";
									}

								?>
							</tr>
						</thead>
						<tbody>
							<?php

								echo "<tr>";

								// Run through the accidents lists
								foreach ( $accidents_count_cbo as $value )
								{
									// Open the chart line
									echo "<th>Quantidade de acidentes de trabalho por CBO/ano</th>";

									// Print the information in the line
									echo "<td alt='" . $value["QUANTIDADE"] . "' title='" . $value["QUANTIDADE"] . "'>" . $value["QUANTIDADE"] . "</td>";
								}

								// Close the chart line
								echo "</tr>";

							?>

						</tbody>
					</table>
				</div><!-- End of .content -->
			</div><!-- End of .box -->
		</div>
		<!-- end: ACCIDENTS BY YEAR CHART -->
	<?php } ?>
	
	<!-- start: ACCIDENTS BY MONTH CHART -->
	<div class="grid_12">
		<div class="box">

			<div class="header">
				<h2><img class="icon" src="<?php echo $this->configuration->HOME_URI;?>/framework/assets/img/icons/packs/fugue/16x16/chart-up-color.png">Top 5 de Acidentes por grupo CNAE</h2>

				<a href="javascript:void(0);" class="menu icon-filter" title="Filtrar informação"></a>
			</div>

			<div class="content" style="height: 250px;">

				<table class=chart data-type=bars data-horizontal=true data-series=columns>
					<thead>
						<tr>
							<?php  

								// Auxiliary variables
								$initial_year = 2010;
								$final_year = 2015;

								// Empty header
								echo "<th></th>";

								// Run the total of years
								for ( $i = $initial_year; $i < $final_year; $i++ )
								{
									echo "<th>" . $i . "</th>";
								}

							?>
						</tr>
					</thead>
					<tbody>
						<?php

							$print_header = 0;
							$descricao = $dashboard_model->get_qtd_accident_by_grupo_cnae();

							foreach ( $descricao as $value )
							{
								for ( $j = $initial_year; $j < $final_year; $j++ )
								{
									// Print the information in the line
									$count = $dashboard_model->get_qtd_accident_by_cnae($j, $value);

									// Check if the group has information and if the header was printed
									if ( $print_header == 0 && $count != 0 )
									{
										echo "<tr><th>". $value ."</th>";
										$print_header = 1;
									}

									echo "<td alt='" . $count . "' title='" . $value . "'>" . $count . "</td>";
								}

								// Close the chart line
								echo "</tr>";
								$print_header = 0;
							}

						?>
					</tbody>	
				</table>
			</div><!-- End of .content -->
			
		</div><!-- End of .box -->
	</div><!-- End of .grid_6 -->
	<!-- end: ACCIDENTS BY MONTH CHART -->

</section><!-- End of #content -->
<!-- end: CONTENT -->