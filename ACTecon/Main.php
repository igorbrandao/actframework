<?php

    /**
     * Import the framework's necessary itens
    */
    include_once 'framework/Application.php';
    include_once 'framework/Configuration.php';
    include_once 'framework/functions/global-functions.php';
    include_once 'controllers/HomeController.php';

    /**
     * Clients main class
    */
    class Main extends Application
    {
        public function __construct( Controller $controller, Configuration $conf )
        {   
            parent::__construct($controller, $conf);
        }

        public function start()
        {
            $this->controller->init();
        }
    }

    // Starts here!
    $main = new Main(
        new HomeController(),
        new Configuration(
            'ACTecon',
            'https://acidente-trabalho-badhokage.c9users.io/ACTecon',
            true,
            'Dashboard',
            'localhost',
            'BD_ACT',
            'root',
            '',
            'utf8'
        )
    );

    $main->start();
?>