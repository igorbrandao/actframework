<!-- start: SIDEBAR -->
	<aside>
		<div class="top">

			<?php

				$my_model = $this->getModel2();

				// Check the model instance
				if ( !isset($my_model) )
				{
					$my_model = $this->mvc_controller->getModel2();
				}

			?>

			<!-- Navigation -->
			<nav><ul class="collapsible accordion">

				<li <?php if ( $this->configuration->ACTIVE_TAB == 'Dashboard' ) echo "class='current'"; ?> >
					<a href="<?php echo join(DIRECTORY_SEPARATOR, array($this->configuration->HOME_URI)); ?>">
						<img src="<?php echo $this->configuration->HOME_URI;?>/framework/assets/img/icons/packs/fugue/16x16/dashboard.png" title="Dashboard" alt="Página inicial" height=16 width=16>
						Dashboard
					</a>
				</li>

				<li <?php if ( $this->configuration->ACTIVE_TAB == 'Users' ) echo "class='current'"; ?> >
					<a href="<?php echo join(DIRECTORY_SEPARATOR, array($this->configuration->HOME_URI, 'UserModule')); ?>">
						<img src="<?php echo $this->configuration->HOME_URI;?>/framework/assets/img/icons/packs/fugue/16x16/users.png" title="Usuários" alt="Usuários" height=16 width=16>
						Usuários
					</a>
				</li>

			</ul></nav><!-- End of nav -->				
		</div><!-- End of .top -->
		
		<div class="bottom sticky">
			<div class="divider"></div>
			<div class="progress">
				<div class="bar" data-title="Usuários cadastrados" data-value="<?php echo $my_model->get_users_count(); ?>" data-max="1000"></div>
				
				<?php

					// UF statistics variable
					$total_accident_youruf			= $my_model->get_accident_count_byUF($this->getUser()->UF);
					$total_accident_uf				= $my_model->get_accident_count_byUF();
					$total_accident_uf_percentage	= round(($total_accident_youruf / $total_accident_uf) * 100, 2);

				?>
				
				<div class="bar" data-title="Acidentes na sua região (<?php echo $total_accident_uf_percentage; ?>%)" 
				data-value="<?php echo $total_accident_youruf ?>" data-max="<?php echo $total_accident_uf; ?>"></div>

				<?php

					// Gender statistics variable
					$total_accident_yourgender			= $my_model->get_accident_count_byGender($this->getUser()->GENDER);
					$total_accident_gender				= $my_model->get_accident_count_byGender();
					$total_accident_gender_percentage	= round(($total_accident_yourgender / $total_accident_gender) * 100, 2);

				?>

				<div class="bar" data-title="Acidentes do seu sexo (<?php echo $total_accident_gender_percentage; ?>%)" 
				data-value="<?php echo $total_accident_yourgender ?>" data-max="<?php echo $total_accident_gender; ?>"></div>
				
				<?php

					// Birth statistics variable
					$total_accident_yourbirth			= $my_model->get_accident_count_byBirthMonth(substr($this->getUser()->BIRTH, 3, 2));
					$total_accident_birth				= $my_model->get_accident_count_byBirthMonth();
					$total_accident_birth_percentage	= round(($total_accident_yourbirth / $total_accident_birth) * 100, 2);

				?>

				<div class="bar" data-title="Acidentes no seu mês de aniversário (<?php echo $total_accident_birth_percentage; ?>%)" 
				data-value="<?php echo $total_accident_yourbirth ?>" data-max="<?php echo $total_accident_birth; ?>"></div>
			</div>
			<div class="divider"></div>
			<!--<div class="buttons">
				<a href="javascript:void(0);" class="button grey open-add-client-dialog">Add New Client</a>
				<a href="javascript:void(0);" class="button grey open-add-client-dialog">Open a Ticket</a>
			</div>-->
		</div><!-- End of .bottom -->
	</aside>
<!-- end: SIDEBAR -->