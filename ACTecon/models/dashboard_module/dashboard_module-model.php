<?php
	include_once 'framework/Analysis.php';
	
	class DashboardModuleModel implements Analysis
	{
		
		/**
		 * Class constructor
		 *
		 * Set the database, controller, parameter and user data.
		 *
		 * @since 0.1
		 * @access public
		 * @param object $db PDO Conexion object
		 * @param object $controller Controller object
		 * @param object $configuration_ Configuration object
		*/
		public function __construct( $db_ = false, $controller_ = null, Configuration $configuration_ )
		{
			// Set DB (PDO)
			$this->db = $db_;

			// Set the controller
			$this->controller = $controller_;

			// Set the main parameters
			$this->mvc_parameters = $this->controller->mvc_parameters;

			// Set user data
			//$this->userdata = $this->controller->userdata;

			// Define the active tab
			$configuration_->ACTIVE_TAB = "Dashboard";
		}

		/**
		 * Get the site's statistcs
		 * 
		 * @since 0.1
		 * @access public
		*/
		public function get_qtd_accident_by_year( $year_ ) 
		{
			// Select the necessary data from DB
			$sql = "SELECT SUM(`QTD_ACIDENTES`) AS QTD FROM `ACT_MES` WHERE `ANO` = " . $year_ . " AND `DATA_FECHA` IS NULL";

			// Execute the sql statement
			$query = $this->db->query($sql);

			// Check if query worked
			if ( ! $query )
				return array();

			// Return data to view
			return $query->fetchColumn(0);
		} // get_qtd_accident_by_year

		/**
		 * Get the site's statistcs
		 * 
		 * @since 0.1
		 * @access public
		*/
		public function get_qtd_accident_by_month( $month_year_ ) 
		{
			// Select the necessary data from DB
			$sql = "SELECT SUM(`QTD_ACIDENTES`) AS QTD FROM `ACT_MES` WHERE `MES` LIKE '%" . $month_year_ . "%' AND `DATA_FECHA` IS NULL";

			// Execute the sql statement
			$query = $this->db->query($sql);

			// Check if query worked
			if ( ! $query )
				return array();

			// Return data to view
			return $query->fetchColumn(0);
		} // get_qtd_accident_by_month

		/**
		 * Get the list of accidents situation by CBO
		 * 
		 * @since 0.1
		 * @access public
		 * 
		 * param $CBO_ => cbo ID
		*/
		public function get_accident_situationlist_byCBO( $CBO_ = null ) 
		{
			// Select accidents related to all genders
			$sql = "SELECT CBO.`ID_SITUACAO`, ST.`DESCRICAO`
				FROM 
					`ACT_CBO` CBO
				INNER JOIN 
					`SITUACAO` AS ST ON ST.`ID_SITUACAO` = CBO.`ID_SITUACAO` 
				WHERE CBO.`DATA_FECHA` IS NULL AND ST.`DATA_FECHA` IS NULL ";

			// Check if the CBO was informed
			if ( !is_null( $CBO_ ) )
			{
				$sql .= " AND CBO.`ID_CBO` = " . $CBO_;
			}

			$sql .= " GROUP BY ST.`ID_SITUACAO`";

			// Execute the query
			$query = $this->db->query($sql);

			// Check if query worked
			if ( ! $query )
				return array();

			// Return data to view
			return $query->fetchAll();
		} // get_accident_situationlist_byCBO
		
		/**
		 * Get the count of accidents situation by CBO
		 * 
		 * @since 0.1
		 * @access public
		 * 
		 * param $CBO_ => cbo ID
		 * param $SITUATION_ => situation ID
		*/
		public function get_accident_situationcount_byCBO( $CBO_ = null, $SITUATION_ = null ) 
		{
			// Select the necessary data from DB
			$sql = "SELECT SUM(`QTD_ACIDENTES`) FROM `ACT_CBO` WHERE `ID_CBO` = " . $CBO_ . " AND 
				`ID_SITUACAO` = " . $SITUATION_ . " AND `DATA_FECHA` IS NULL";

			// Execute the sql statement
			$query = $this->db->query($sql);

			// Check if query worked
			if ( ! $query )
				return array();

			// Return data to view
			return $query->fetchColumn(0);
		} // get_accident_situationcount_byCBO

		/**
		 * Get the count of accidents situation by year and CBO
		 * 
		 * @since 0.1
		 * @access public
		 * 
		 * param $CBO_ => cbo ID
		*/
		public function get_accident_count_byYearCBO( $CBO_ = null ) 
		{
			// Select the necessary data from DB
			$sql = "SELECT `ID_CBO`, `ANO`, SUM(`QTD_ACIDENTES`) AS QUANTIDADE FROM `ACT_CBO` 
				WHERE `ID_CBO` = " . $CBO_ . " AND `DATA_FECHA` IS NULL GROUP BY `ANO`";

			// Execute the sql statement
			$query = $this->db->query($sql);

			// Check if query worked
			if ( ! $query )
				return array();

			// Return data to view
			return $query->fetchAll();
		} // get_accident_count_byYearCBO
		
		/**
		 * Get the general accidents average by CBO
		 * 
		 * @since 0.1
		 * @access public
		 * 
		 * param $CBO_ => cbo ID
		*/
		public function get_accident_avg( $Item_ = null ) 
		{
			// Select the necessary data from DB
			$sql = "SELECT AVG(`QTD_ACIDENTES`) FROM `ACT_CBO` WHERE `DATA_FECHA` IS NULL ";

			// Check if the CBO was informed
			if ( !is_null( $Item_ ) )
			{
				$sql .= "AND `ID_CBO` = " . $Item_;
			}

			// Execute the sql statement
			$query = $this->db->query($sql);

			// Check if query worked
			if ( ! $query )
				return array();

			// Return data to view
			return round($query->fetchColumn(0), 2);
		} // get_accident_avg_byCBO
		
		/**
		 * Get the general accidents median by CBO
		 * 
		 * @since 0.1
		 * @access public
		 * 
		 * param $CBO_ => cbo ID
		*/
		public function get_accident_median( $Item_ = null ) 
		{
			// Select the necessary data from DB
			$sql = "SELECT `QTD_ACIDENTES` FROM `ACT_CBO` WHERE `DATA_FECHA` IS NULL ";

			// Check if the CBO was informed
			if ( !is_null( $Item_ ) )
			{
				$sql .= "AND `ID_CBO` = " . $Item_;
			}

			// Execute the sql statement
			$query = $this->db->query($sql);

			// Check if query worked
			if ( ! $query )
				return array();

			$median_array = array_column($query->fetchAll(), 'QTD_ACIDENTES');

			// Calculates the median from data collection
			$median = array_median($median_array);

			// Return data to view
			return $median;
		} // get_accident_median_byCBO
		
		/**
		 * Get the general accidents median by CBO
		 * 
		 * @since 0.1
		 * @access public
		 * 
		 * param $CBO_ => cbo ID
		*/
		public function get_accident_mode( $Item_ = null ) 
		{
			// Select the necessary data from DB
			$sql = "SELECT `QTD_ACIDENTES` FROM `ACT_CBO` WHERE `DATA_FECHA` IS NULL ";

			// Check if the CBO was informed
			if ( !is_null( $Item_ ) )
			{
				$sql .= "AND `ID_CBO` = " . $Item_;
			}

			// Execute the sql statement
			$query = $this->db->query($sql);

			// Check if query worked
			if ( ! $query )
				return array();

			$mode_array = array_column($query->fetchAll(), 'QTD_ACIDENTES');

			// Calculates the mode from data collection
			$median = array_mode($mode_array);

			// Return data to view
			return $median;
		} // get_accident_mode_byCBO
		
		/**
		 * Get the general accidents average by CBO
		 * 
		 * @since 0.1
		 * @access public
		 * 
		 * param $CID => cid ID
		*/
		public function get_qtd_accident_by_grupo_cnae() 
		{
			// Select the necessary data from DB
			$sql = "SELECT DISTINCT `GRUPOCNAE` FROM  `CNAE` WHERE `DATA_FECHA` IS NULL LIMIT 23, 5";

			// Execute the sql statement
			$query = $this->db->query($sql);

			// Check if query worked
			if ( ! $query )
				return array();


			// Return data to view
			return array_column($query->fetchAll(), 'GRUPOCNAE');
		} // get_qtd_accident_by_partes_corpo
		
		/**
		 * Get the site's statistcs
		 * 
		 * @since 0.1
		 * @access public
		*/
		public function get_qtd_accident_by_cnae( $year_, $descricao_ ) 
		{
			// Select the necessary data from DB
			$sql = " SELECT COALESCE( SUM(  `QTD_ACIDENTES` ) , 0 ) AS QTD 
				FROM 
					`ACT_ATIVIDADE_ECONOMICA` AS ACTE
				INNER JOIN `CNAE` AS CNAE ON CNAE.`CNAE` = ACTE.`ID_CNAE` 
				WHERE 
					ACTE.`ANO` = " . $year_ . " AND CNAE.`GRUPOCNAE` LIKE '%" . $descricao_ . "%' AND ACTE.`DATA_FECHA` IS NULL";

			// Execute the sql statement
			$query = $this->db->query($sql);

			// Check if query worked
			if ( ! $query )
				return array();

			// Return data to view
			return $query->fetchColumn(0);
		} // get_qtd_accident_by_corpo
	}

?>