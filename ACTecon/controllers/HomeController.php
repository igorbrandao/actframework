<?php
	
	/**
     * Import the framework's necessary itens
    */
    include_once 'framework/controllers/Controller.php';

	/**
	 * Home - Index Controller
	 *
	 * @since 0.1
	*/
	class HomeController extends Controller
	{
		/**
		 * Load the page "https://www.site.com/home-view.php"
		*/
		public function index( )
		{
			// Page title
			$this->title = $this->configuration->APP_NAME . ' - Dashboard';
			$this->configuration->ACTIVE_TAB = "Dashboard";

			// Function parameter
			$this->mvc_parameters = ( func_num_args() >= 1 ) ? func_get_arg(0) : array();

			// Load model
			$this->setModel( $this->load_model('dashboard_module/dashboard_module-model') );
			$this->setModel2( $this->load_model('home_module/home_module-model') );

			// Set the page's view and breadcrumb
			$this->page->views["breadcrumb"] = $_SERVER['DOCUMENT_ROOT'] . '/' . $this->configuration->APP_NAME . '/views/home/_breadcrumb_home.php';
			$this->page->views["view"] = $_SERVER['DOCUMENT_ROOT'] . '/' . $this->configuration->APP_NAME . '/views/home/home-view.php';
			
			return $this->page;
		} // index
		
	} // class HomeController
?>