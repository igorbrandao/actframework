<!-- start: CONTENT -->
<section id="content" class="container_12 clearfix" data-sort=true>

	<?php

		// Handle multiple models
		$dashboard_model = $this->getModel();
		$user_model = $this->getModel2();

		// Check the model instance
		if ( !isset($dashboard_model) )
		{
			$dashboard_model = $this->mvc_controller->getModel();
		}

		if ( !isset($user_model) )
		{
			$user_model = $this->mvc_controller->getModel2();
		}

	?>

	<!--<ul class="stats not-on-phone">
		<li title="Usuários cadastrados">
			<strong><?php //echo $user_model->get_users_count(); ?></strong>
			<small>Usuários cadastrados</small>
		</li>
		<li title="Usuários cadastrados">
			<strong><?php //echo $user_model->get_users_count(); ?></strong>
			<small>Usuários cadastrados</small>
		</li>
		<li title="Usuários cadastrados">
			<strong><?php //echo $user_model->get_users_count(); ?></strong>
			<small>Usuários cadastrados</small>
		</li>
		<li title="Usuários cadastrados">
			<strong><?php //echo $user_model->get_users_count(); ?></strong>
			<small>Usuários cadastrados</small>
		</li>
	</ul>--><!-- End of ul.stats -->

	<h1 class="grid_12 margin-top no-margin-top-phone" title="<?php echo $this->configuration->APP_NAME; ?> - Dashboard">
		<?php echo $this->configuration->APP_NAME; ?> - Dashboard
	</h1>

	<div class="grid_12">

		<!-- start: GENERAL AVG -->
		<div class="content">
			<div class="spacer"></div>
			<div class="full-stats">
				<div class="stat hlist" data-list='[{"val":<?php echo $dashboard_model->get_accident_avg(); ?>,
				"title":"Média nacional dos acidentes","color":"green"},

				{"val":<?php echo $dashboard_model->get_accident_median(); ?>,"title":"Mediana nacional dos acidentes","color":"red"},

				{"val":<?php echo $dashboard_model->get_accident_mode(); ?>,"title":"Moda nacional dos acidentes"}]' data-flexiwidth=true></div>
			</div>
		</div><!-- End of .content -->
		<!-- end: GENERAL AVG -->

		<?php if ( isset($this->getUser()->UF) && strcmp($this->getUser()->UF, "") != 0 ) { ?>
			<!-- start: AVG BY CBO -->
			<div class="content">
				<div class="spacer"></div>
				<div class="full-stats">
					<div class="stat hlist" data-list='[{"val":<?php echo $dashboard_model->get_accident_avg($this->getUser()->UF); ?>,
					"title":"Média de acidentes em <?php echo $this->getUser()->UF; ?>","color":"green"},

					{"val":<?php echo $dashboard_model->get_accident_median($this->getUser()->UF); ?>,
					"title":"Mediana de acidentes em <?php echo $this->getUser()->UF; ?>","color":"red"},

					{"val":<?php echo $dashboard_model->get_accident_mode( $this->getUser()->UF ); ?>,"title":"Moda de acidentes em <?php echo $this->getUser()->UF; ?>"}]' data-flexiwidth=true></div>
				</div>
			</div><!-- End of .content -->
			<!-- end: AVG BY CBO -->
		<?php } ?>

	</div>

	<!-- start: SPOT MAP -->
	<div class="grid_12">
		<div class="box">

			<div class="header">
				<h2><img class="icon" src="<?php echo $this->configuration->HOME_URI;?>/framework/assets/img/icons/packs/fugue/16x16/chart-up-color.png">Gráfico de mancha</h2>

				<a href="javascript:void(0);" class="menu icon-filter" title="Filtrar informação"></a>
			</div>

			<div class="content" style="height: 800px;">

				</br>
		
				<?php

					// Receive the serialized information
					$UF_counts = $dashboard_model->serialize_count_byUF($dashboard_model->get_count_byUF());

				?>

				<!-- Map component -->
				<iframe width="100%" height="95%" src="<?php echo $this->configuration->HOME_URI;?>/framework/assets/map/index.php?infos=<?php echo $UF_counts ?>" frameborder="0" allowfullscreen></iframe>

			</div><!-- End of .content -->
		</div><!-- End of .box -->
	</div>
	<!-- end: SPOT MAP -->

	<!-- start: ACCIDENTS BY YEAR CHART -->
	<div class="grid_12">
		<div class="box">

			<div class="header">
				<h2><img class="icon" src="<?php echo $this->configuration->HOME_URI;?>/framework/assets/img/icons/packs/fugue/16x16/chart-up-color.png">Quantidade de acidentes totais entre 1997 - 2014</h2>

				<a href="javascript:void(0);" class="menu icon-filter" title="Filtrar informação"></a>
			</div>

			<div class="content" style="height: 300px;">
				<table class=chart >
					<thead>
						<tr>
							<?php  

								// Auxiliary variables
								$initial_year = 1997;
								$final_year = 2015;

								// Empty header
								echo "<th></th>";

								// Run the total of years
								for ( $i = $initial_year; $i < $final_year; $i++ )
								{
									echo "<th>" . $i . "</th>";
								}

							?>
						</tr>
					</thead>
					<tbody>
						<?php

							echo "<tr>";

							// Run the total of years
							for ( $i = $initial_year; $i < $final_year; $i++ )
							{
								// Open the chart line
								echo "<th>Quantidade de acidentes de trabalho por ano</th>";

								// Print the information in the line
								$count = $dashboard_model->get_qtd_accident_by_year($i);
								echo "<td alt='" . $count . "' title='" . $count . "'>" . $count . "</td>";
							}

							// Close the chart line
							echo "</tr>";

						?>

					</tbody>	
				</table>
			</div><!-- End of .content -->
		</div><!-- End of .box -->
	</div>
	<!-- end: ACCIDENTS BY YEAR CHART -->

	<!-- start: ACCIDENTS BY MONTH CHART -->
	<div class="grid_6">
		<div class="box">

			<div class="header">
				<h2><img class="icon" src="<?php echo $this->configuration->HOME_URI;?>/framework/assets/img/icons/packs/fugue/16x16/chart-up-color.png">Quantidade de acidentes por mês</h2>

				<a href="javascript:void(0);" class="menu icon-filter" title="Filtrar informação"></a>
			</div>

			<div class="content" style="height: 250px;">

				<?php

					$months = array(
					    'Janeiro',
					    'Fevereiro',
					    'Março',
					    'Abril',
					    'Maio',
					    'Junho',
					    'Julho',
					    'Agosto',
					    'Setembro',
					    'Outubro',
					    'Novembro',
					    'Dezembro'
					);

				?>

				<table class=chart data-type=bars>
					<thead>
						<tr>
							<th></th>
							<?php
								foreach ($months as $value) {
								    echo "<th>" . $value . "</th>";
								}
							?>
						</tr>
					</thead>
					<tbody>
						<?php

							echo "<tr>";

							// Run the total of years
							foreach ( $months as $value )
							{
								// Open the chart line
								echo "<th>Quantidade de acidentes de trabalho por mês</th>";

								// Print the information in the line
								$count = $dashboard_model->get_qtd_accident_by_month($value);
								echo "<td alt='" . $count . "' title='" . $count . "'>" . $count . "</td>";
							}

							// Close the chart line
							echo "</tr>";

						?>
					</tbody>	
				</table>
			</div><!-- End of .content -->
			
		</div><!-- End of .box -->
	</div><!-- End of .grid_6 -->
	<!-- end: ACCIDENTS BY MONTH CHART -->

</section><!-- End of #content -->
<!-- end: CONTENT -->