<?php
	
	include_once 'framework/Analysis.php';
	
	class DashboardModuleModel implements Analysis
	{
		/**
		 * Class constructor
		 *
		 * Set the database, controller, parameter and user data.
		 *
		 * @since 0.1
		 * @access public
		 * @param object $db PDO Conexion object
		 * @param object $controller Controller object
		 * @param object $configuration_ Configuration object
		*/
		public function __construct( $db_ = false, $controller_ = null, Configuration $configuration_ )
		{
			// Set DB (PDO)
			$this->db = $db_;

			// Set the controller
			$this->controller = $controller_;

			// Set the main parameters
			$this->mvc_parameters = $this->controller->mvc_parameters;

			// Set user data
			//$this->userdata = $this->controller->userdata;

			// Define the active tab
			$configuration_->ACTIVE_TAB = "Dashboard";
		}

		/**
		 * Get the site's statistcs
		 * 
		 * @since 0.1
		 * @access public
		*/
		public function get_qtd_accident_by_year( $year_ ) 
		{
			// Select the necessary data from DB
			$sql = "SELECT SUM(`QTD_ACIDENTES`) AS QTD FROM `ACT_MES` WHERE `ANO` = " . $year_ . " AND `DATA_FECHA` IS NULL";

			// Execute the sql statement
			$query = $this->db->query($sql);

			// Check if query worked
			if ( ! $query )
				return array();

			// Return data
			return $query->fetchColumn(0);
		} // get_qtd_accident_by_year

		/**
		 * Get the site's statistcs
		 * 
		 * @since 0.1
		 * @access public
		*/
		public function get_qtd_accident_by_month( $month_year_ ) 
		{
			// Select the necessary data from DB
			$sql = "SELECT SUM(`QTD_ACIDENTES`) AS QTD FROM `ACT_MES` WHERE `MES` LIKE '%" . $month_year_ . "%' AND `DATA_FECHA` IS NULL";

			// Execute the sql statement
			$query = $this->db->query($sql);

			// Check if query worked
			if ( ! $query )
				return array();

			// Return data
			return $query->fetchColumn(0);
		} // get_qtd_accident_by_month
		
		/**
		 * Get the general accidents average by CBO
		 * 
		 * @since 0.1
		 * @access public
		 * 
		 * param $CBO_ => cbo ID
		*/
		public function get_accident_avg( $Item_ = null ) 
		{
			// Select the necessary data from DB
			$sql = "SELECT AVG(`QTD_ACIDENTES`) FROM `ACT_UF` WHERE `DATA_FECHA` IS NULL ";

			// Check if the CBO was informed
			if ( !is_null( $Item_ ) )
			{
				$sql .= "AND `UF` LIKE '%" . $Item_ . "%' ";
			}

			// Execute the sql statement
			$query = $this->db->query($sql);

			// Check if query worked
			if ( ! $query )
				return array();

			// Return data
			return round($query->fetchColumn(0), 2);
		} // get_accident_avg_byCBO
		
		/**
		 * Get the general accidents median by CBO
		 * 
		 * @since 0.1
		 * @access public
		 * 
		 * param $CBO_ => cbo ID
		*/
		public function get_accident_median( $Item_ = null ) 
		{
			// Select the necessary data from DB
			$sql = "SELECT `QTD_ACIDENTES` FROM `ACT_UF` WHERE `DATA_FECHA` IS NULL ";

			// Check if the CBO was informed
			if ( !is_null( $Item_ ) )
			{
				$sql .= "AND `UF` LIKE '%" . $Item_ . "%' ";
			}

			// Execute the sql statement
			$query = $this->db->query($sql);

			// Check if query worked
			if ( ! $query )
				return array();

			$median_array = array_column($query->fetchAll(), 'QTD_ACIDENTES');

			// Calculates the median from data collection
			$median = array_median($median_array);

			// Return data
			return $median;
		} // get_accident_median_byCBO
		
		/**
		 * Get the general accidents median by CBO
		 * 
		 * @since 0.1
		 * @access public
		 * 
		 * param $CBO_ => cbo ID
		*/
		public function get_accident_mode( $Item_ = null ) 
		{
			// Select the necessary data from DB
			$sql = "SELECT `QTD_ACIDENTES` FROM `ACT_UF` WHERE `DATA_FECHA` IS NULL ";

			// Check if the CBO was informed
			if ( !is_null( $Item_ ) )
			{
				$sql .= "AND `UF` LIKE '%" . $Item_ . "%' ";
			}

			// Execute the sql statement
			$query = $this->db->query($sql);

			// Check if query worked
			if ( ! $query )
				return array();

			$mode_array = array_column($query->fetchAll(), 'QTD_ACIDENTES');

			// Calculates the mode from data collection
			$median = array_mode($mode_array);

			// Return data
			return $median;
		} // get_accident_mode_byCBO
		
		/**
		 * Get the accidents count by UF and year
		 * 
		 * @since 0.1
		 * @access public
		 * 
		 * @param $YEAR_ => year
		*/
		public function get_count_byUF( $YEAR_ = null ) 
		{
			// Select the necessary data from DB
			$sql = "SELECT `UF`, SUM(`QTD_ACIDENTES`) AS QUANTIDADE FROM `ACT_UF` WHERE `DATA_FECHA` IS NULL";

			// Check if year was informed
			if ( !is_null($YEAR_) )
			{
				// Add another where condition
				$sql .= " AND `ANO` = " . $YEAR_;
			}

			$sql .= " GROUP BY `UF` ";

			// Execute the query
			$query = $this->db->query($sql);

			// Check if query worked
			if ( ! $query )
				return array();

			// Return data
			return $query->fetchAll();
		} // get_user_type_list

		/**
		 * Converts the UF array into a queryString
		 * 
		 * @since 0.1
		 * @access public
		 * 
		 * @param $array_UF_ => array with UF counts
		*/
		public function serialize_count_byUF( $array_UF_ ) 
		{
			// String result
			$queryString = "";

			// Get the UF initials
			$array_UF_initials = $this->get_UF_initials();

			// Counter
			$i = 0;

			// Run the total of years
			foreach ( $array_UF_ as $value )
			{
				// Check the array index
				if ( isset($array_UF_initials[$i]) )
				{
					$queryString .= strtolower($array_UF_initials[$i]);
				}

				if ( isset($value["QUANTIDADE"]) )
				{
					$queryString .= "//" . $value["QUANTIDADE"] . "@@";
				}

				// Increment the counter
				$i++;
			}

			// Return the serialized queryString
			return $queryString;

		} // serialize_count_byUF
		
		/**
		 * Get the UF initials
		 * 
		 * @since 0.1
		 * @access public
		 * 
		*/
		public function get_UF_initials() 
		{
			// Select the necessary data from DB
			$sql = "SELECT `UF` FROM `UF` WHERE `DATA_FECHA` IS NULL";

			// Execute the query
			$query = $this->db->query($sql);

			// Check if query worked
			if ( ! $query )
				return array();

			// Convert into a unidimensional array
			$UF_array = array_column($query->fetchAll(), 'UF');

			// Return data
			return $UF_array;
		} // serialize_count_byUF
	}

?>