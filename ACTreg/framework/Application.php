<?php

    /**
     * Import the framework's necessary itens
    */
    include_once 'Configuration.php';
    include_once 'functions/global-functions.php';

    /* Framework constants */
    define('APP_NAME_CONST', 'ACTreg');
    define('BASE_PATH', realpath(dirname(__FILE__)));
    define('HOME_URI', rtrim("http://" . $_SERVER['SERVER_NAME'] . $_SERVER['REQUEST_URI'], "/"));

    /* Framework definitions */
	define( 'ADMIN', "1" );
	define( 'COMMON_USER', "2" );

    /**
     * Abstract class Application
     * handle the necessary configuration to run a new instance
     * 
     * @since 0.1
    */
    abstract class Application
    {
        /**
         * Application attributes
        */
        public $controller;
        public $configuration;
        
        /**
         * Application constructor 
        */
        public function __construct( Controller $controller_, Configuration $configuration_ )
        {
            // Receive the controller instance
            $this->controller = $controller_;
            
            // Set-up the configuration to the framework
            $this->configuration = $configuration_;
            
            // Set-up the configuration to the application
            $this->controller->configuration = $configuration_;

            // Set-up the application itself
            $this->loader();
        }

        /**
         * Set-up the initial configuration
        */
        private function loader()
        {
            // PROTECT FROM DIRECT ACCESS
        	if ( ! isset($this->configuration->ABSPATH) ) exit;
            
        	// START SESSION
        	session_start();

        	// CHOOSE THE OPERATION MODE
        	if ( ! isset($this->configuration->DEBUG) || $this->configuration->DEBUG === false )
        	{
        		// HIDDE ALL ERRORS
        		error_reporting(0);
        		ini_set("display_errors", 0); 
        	}
        	else
        	{
        		// SHOW ALL ERRORS
        		error_reporting(E_ALL);
        		ini_set("display_errors", 1); 
        	}
        }

        /**
         * Abstract start method
        */
        abstract public function start();
    }

?>