<!DOCTYPE html>
    <html>
        <head>
            <title>Mapa do Brasil</title>
            <meta charset="utf-8" />
            <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
            <link rel="stylesheet" href="assets/css/main.css" type="text/css" media="screen" />
            <script src="assets/js/jquery-1.10.2.min.js" type="text/javascript"></script>
            <script src="assets/js/jquery-jvectormap-1.2.2.min.js" type="text/javascript"></script>
            <script src="assets/js/brazil.js" type="text/javascript"></script>
        </head>

        <body>

            <div id="container">
                <div id="hovered-region">ESTADO SELECIONADO: <span>??</span></div>
                <div id="clicked-region">NÚMERO DE ACIDENTES: <span>??</span></div>
                <div id="brazil-map"></div>
            </div>

            <?php
        
                // Auxiliary arrays
                $array_UF = array();
                $array_Count = array();
                $array_Interval = array();
        
                // Status color
                $status_color = array();
                $status_color[0] = "#c6efce";   //!< Interval 0 (GREEN)     - POUCOS ACIDENTES
                $status_color[1] = "#ffeb9c";   //!< Interval 1 (YELLOW)    - MODERADO
                $status_color[2] = "#ffcc99";   //!< Interval 2 (ORANGE)    - 
                $status_color[3] = "#ffc7ce";   //!< Interval 3 (RED)       - MUITOS ACIDENTES cce1e6
        
                // Statistics variables
                $intervals_count = sizeof($status_color);
                $intervals_length = 0;
                $min_val = 0;
                $max_val = 0;
        
                // Check if the map receive the serialized data
                if ( isset($_GET["infos"]) && strcmp($_GET["infos"], "") != 0 )
                {
                    // Explode the info by UF
                    $bucket01 = explode("@@", $_GET["infos"]);
                    $bucket01_count = sizeof($bucket01);
        
                    // Run through the generated buckets
                    for ( $i = 0; $i < $bucket01_count - 1; $i++ )
                    {
                        // Split the UF name and the count
                        $bucket02 = explode("//", $bucket01[$i]);
        
                        // Add the information for each array
                        $array_UF[$i] = $bucket02[0];
                        $array_Count[$i] = $bucket02[1];
                    }
        
                    // Check if the information was load
                    if ( !empty($array_UF) && !empty($array_Count) )
                    {
                        // Get the max value
                        $max_val = max($array_Count);
        
                        // Get the min value
                        $min_val = min($array_Count);
        
                        // Calculates the interval length
                        $intervals_length = (int)(($max_val - $min_val) / $intervals_count);
        
                        /*echo "</br>XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX</br>";
                        echo "Max: " . $max_val . "</br>";
                        echo "Min: " . $min_val . "</br>";
                        echo "Interval length: " . $intervals_length;
                        echo "Interval count: " . $intervals_count;
                        echo "</br>XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX</br>";*/
        
                        // Calculates the data interval
                        $data_count = sizeof($array_Count);
        
                        // Run through the generated buckets
                        for ( $i = 0; $i < $data_count - 1; $i++ )
                        {
                            // Calculates the interval
                            $array_Interval[$array_UF[$i]] = (int)( ($array_Count[$i] - $min_val) / $intervals_length );
        
                            // If the value is max, reduce the cathegory
                            if ( $array_Interval[$array_UF[$i]] == $intervals_count )
                            {
                                $array_Interval[$array_UF[$i]] = ($intervals_count) - 1;
                            }
        
                            //echo $intervals_count . ") " . $array_Count[$i] . " - " . $min_val . " / " . $intervals_length . " = " . $array_Interval[$array_UF[$i]] . "</br>";
                        }
                    }
                }
                else
                {
                    echo "Não foi possível carregar as informações para o gráfico de mancha";
                }
        
            ?>

            <script type="text/javascript">

                $(function () {
                  
                  var map_settings = {
                      map: 'brazil',
                      zoomButtons: false,
                      zoomMax: 1,
                      regionStyle: {
                          initial: {
                              'fill-opacity': 0.9,
                              stroke: '#000',
                              'stroke-width': 100,
                              'stroke-opacity': 1
                          },
                          hover: {
                              fill: '#cce1e6'
                          }
                      },
                      backgroundColor: '#00709a',
                      series: {
                          regions: [{
                              values: {
                                  // Região Norte
                                  ac: '<?php echo $status_color[$array_Interval['ac']]; ?>',
                                  am: '<?php echo $status_color[$array_Interval['am']]; ?>',
                                  ap: '<?php echo $status_color[$array_Interval['ap']]; ?>',
                                  pa: '<?php echo $status_color[$array_Interval['pa']]; ?>',
                                  ro: '<?php echo $status_color[$array_Interval['ro']]; ?>',
                                  rr: '<?php echo $status_color[$array_Interval['rr']]; ?>',
                                  to: '<?php echo $status_color[$array_Interval['to']]; ?>',
                                  // Região Nordeste
                                  al: '<?php echo $status_color[$array_Interval['al']]; ?>',
                                  ba: '<?php echo $status_color[$array_Interval['ba']]; ?>',
                                  ce: '<?php echo $status_color[$array_Interval['ce']]; ?>',
                                  ma: '<?php echo $status_color[$array_Interval['ma']]; ?>',
                                  pb: '<?php echo $status_color[$array_Interval['pb']]; ?>',
                                  pe: '<?php echo $status_color[$array_Interval['pe']]; ?>',
                                  pi: '<?php echo $status_color[$array_Interval['pi']]; ?>',
                                  rn: '<?php echo $status_color[$array_Interval['rn']]; ?>',
                                  se: '<?php echo $status_color[$array_Interval['se']]; ?>',
                                  // Região Centro-Oeste
                                  df: '<?php echo $status_color[$array_Interval['ac']]; ?>',
                                  go: '<?php echo $status_color[$array_Interval['ac']]; ?>',
                                  ms: '<?php echo $status_color[$array_Interval['ac']]; ?>',
                                  mt: '<?php echo $status_color[$array_Interval['ac']]; ?>',
                                  // Região Sudeste
                                  es: '<?php echo $status_color[$array_Interval['df']]; ?>',
                                  mg: '<?php echo $status_color[$array_Interval['mg']]; ?>',
                                  rj: '<?php echo $status_color[$array_Interval['rj']]; ?>',
                                  sp: '<?php echo $status_color[$array_Interval['sp']]; ?>',
                                  // Região Sul
                                  pr: '<?php echo $status_color[$array_Interval['pr']]; ?>',
                                  rs: '<?php echo $status_color[$array_Interval['rs']]; ?>',
                                  sc: '<?php echo $status_color[$array_Interval['sc']]; ?>',
                              },
                              attribute: 'fill'
                          }]
                      },
                      container: $('#brazil-map'),
                      onRegionClick: function (event, code) {
                          $('#clicked-region span').text(code);
                      },
                      onRegionOver: function (event, code) {
                          $('#hovered-region span').text(code);
                      }
                  };
        
                  map = new jvm.WorldMap($.extend(true, {}, map_settings));

              });

            </script>

        </body>

    </html>