<?php

	/**
     * Import the framework's necessary itens
    */
    include BASE_PATH . '/Page.php';		//!< Page definition
    include BASE_PATH . '/DAO.php';			//!< DB access and operations
    include BASE_PATH . '/UserLogin.php';	//!< Security module and user access

    /** 
     * Class MainController
     * 
     * @since 0.1
    */
    abstract class Controller
    {
        /** 
         * Pages attributes
        */
        public $db;
        public $phpass;
        public $title;
        public $page;
		public $configuration;

		public $not_found_page;
        public $login_page;

        /**
	     * MVC attributes
	    */
	    private $mvc_controller;
	    private $mvc_action;
	    private $mvc_parameters;
	    private $mvc_model;
	    private $mvc_model2;
	    private $mvc_model3;
	    private $user_data;

	    /**
	     * Get and set for model 
	    */
	    public function getModel()
	    {
	    	return $this->mvc_model;
	    }
	    public function setModel( $model_ )
	    {
	    	$this->mvc_model = $model_;
	    }
	    public function getModel2()
	    {
	    	return $this->mvc_model2;
	    }
	    public function setModel2( $model_ )
	    {
	    	$this->mvc_model2 = $model_;
	    }
	    public function getModel3()
	    {
	    	return $this->mvc_model3;
	    }
	    public function setModel3( $model_ )
	    {
	    	$this->mvc_model3 = $model_;
	    }

	    /**
	     * Get and set for user
	    */
	    public function getUser()
	    {
	    	return $this->user_data;
	    }
	    public function setUser( $user_ )
	    {
	    	$this->user_data = $user_;
	    }

        /**
         * Controller constructor
        */
        public function __construct( $parameters_ = array() )
        {
            // Create a new page instance
            $this->page = new Page();

            // Define the not found page
            $this->not_found_page = BASE_PATH . '/includes/404.php';

            // Define the login page
            $this->login_page = BASE_PATH . '/views/login_module/login-view.php';
        }

        /**
         * Function to handle 404 error (not found) 
        */
        protected function getNotFoundPage()
        {
            $myPage = new Page();
            $myPage->views = array();
            $myPage->views[0] = $this->not_found_page; 
       
            return $myPage;
        }

        /**
         * Function to redirect the user to login page
        */
        protected function getLoginPage()
        {
            $myPage = new Page();
            $myPage->views = array();
            $myPage->views[0] = $this->login_page; 

            return $myPage;
        }

        /**
         * Function to redirect the user to dashboard (home)
        */
        protected function getHomePage()
        {
        	// Redirection
			echo '<meta http-equiv="Refresh" content="0; url=' . $this->configuration->HOME_URI . '">';
			echo '<script type="text/javascript">window.location.href = "' . $this->configuration->HOME_URI . '";</script>';
        }

        /**
	     * Method to parse the URL into controllers
	    */
	    public function init()
	    {
	    	// Call the function to parse the URL
            $this->get_url_data();

            // Create a new DAO instance
            $this->db = new DAO( $this->configuration );

            // Create a new User instance
            $obj_user = new UserLogin( $this->db, $this->configuration );
            $this->user_data = $obj_user->check_userlogin();

			// Check if it's a login operation
            if ( isset($this->mvc_controller) && isset($this->mvc_action) && 
            	strcmp($this->mvc_controller, "ModuloLoginController") == 0 && strcmp($this->mvc_action, "login_auth") == 0 )
            {
            	// Call login method
            	$user_data = $obj_user->doLogin();

            	// Redirect the user to dashboard
            	$this->page = $this->getHomePage();
            }
            // Check if it's a logout operation
            else if ( isset($_GET["action"]) && strcmp($_GET["action"], "logout") == 0 )
            {
            	// Call logout method
            	$obj_user->logout();

            	// Redirect the user to the login page
            	$this->page = $this->getLoginPage();
            }
            // Check if user is logged
            else if ( $this->user_data == false )
            {
            	// Redirect the user to the login page
            	$this->page = $this->getLoginPage();
            }
			else
			{
		        // Check if it's exist a controller
		    	if ( isset($this->mvc_controller) )
				{
					// Check if exists the controller's associated class
					if ( class_exists($this->mvc_controller) )
					{
						// Create the controller's instance
						$this->mvc_controller = new $this->mvc_controller( $this->mvc_parameters );
						$this->mvc_controller->configuration = $this->configuration;
	
						// Remove invalid characters fromethod name.
						$this->mvc_action = preg_replace( '/[^a-zA-Z]/i', '', $this->mvc_action );
	
						// If the method exists, run it and send the parameters
						if ( method_exists( $this->mvc_controller, $this->mvc_action ) )
						{
							$this->page = $this->mvc_controller->{$this->mvc_action}( $this->mvc_parameters );
						} // method_exists
	
						// Without action, calls index method
						if ( !$this->mvc_action && method_exists( $this->mvc_controller, 'index' ) ) 
						{
							$this->page = $this->mvc_controller->index( $this->mvc_parameters );
						} // ! $this->mvc_action 
					}
					else
					{
						$this->page = $this->getNotFoundPage();
					}
				}
				else
				{
					$this->page = $this->index($this->mvc_parameters);
				}
			}

			// Include all page elements
			$this->requireViews();
        }

        /**
	     * Method to parse the URL and extract it's informations
	    */
	    public function get_url_data() 
		{
			// Verify if path parameter was sent
			if ( isset( $_GET['path'] ) ) 
			{
				// Catch $_GET['path']
				$path = $_GET['path'];

				// Data cleaning
				$path = rtrim($path, '/');
				$path = filter_var($path, FILTER_SANITIZE_URL);

				// Generate parameter array
				$path = explode('/', $path);

				// Set properties
				$this->mvc_controller	= chk_array( $path, 0 );

				// Fix to APP_NAME duplicated in URL
				if ( strcmp($this->mvc_controller, $this->configuration->APP_NAME) == 0 )
				{
					$this->mvc_controller	= chk_array( $path, 1 );
					$this->mvc_action		= chk_array( $path, 2 );
				}
				else
				{
					$this->mvc_action		= chk_array( $path, 1 );
				}

				// Add a suffix to controller name
				$this->mvc_controller	.= 'Controller';

				// Set parameters
				if ( chk_array( $path, 2 ) ) 
				{
					unset( $path[0] );
					unset( $path[1] );

					// Parameters always come after action
					$this->mvc_parameters = array_values( $path );
				}
			}
		}

		/**
	     * Function to require all view element
	    */
		public function requireViews()
	    {
	        foreach ( $this->page->views as $value ) 
	        {
	            require_once $value;
	        }
	    }

		/**
		 * Load model
		 *
		 * Load models present in the folder /models/.
		 *
		 * @since 0.1
		 * @access public
		*/
		public function load_model( $model_name = false )
		{
			// A file should be sent
			if ( ! $model_name ) return;

			// Make sure that model's name is lower-case
			$model_name =  strtolower( $model_name );

			// Include the file
			$model_path = BASE_PATH . '/models/' . $model_name . '.php';

			// Check if file exists in the framework
			if ( !file_exists( $model_path ) ) 
			{
				// If not exist in the framework, search in the instance
				$model_path = str_replace("framework", "", BASE_PATH) . 'models/' . $model_name . '.php';

				// If it doesn't exist neither in the framework, neither in the instance, return
				if ( !file_exists( $model_path ) ) 
				{
					return;
				}
			}

			// Include the file
			require_once $model_path;

			// Remove the file path (if exists)
			$model_name = explode('/', $model_name);

			// Get just the final of the path
			$model_name = end( $model_name );

			// Remove invalid characters from file name
			$model_name = preg_replace( '/[^a-zA-Z0-9]/is', '', $model_name );

			// Check if class exists
			if ( class_exists( $model_name ) )
			{
				// Check if db instance is null
				if ( is_null( $this->db ) == false )
				{
					// Pass the database object to model and return a object from class
					return new $model_name( $this->db, $this, $this->configuration );
				}
				else
				{
					// Pass a new database instance to model and return a object from class
					return new $model_name( new DAO( $this->configuration ), $this, $this->configuration );
				}
			}

			return;
		} // load_model

        /**
	     * Function to define the obligation to implement the index() method 
	     * into the controllers
	    */
		public abstract function index();
    }

?>