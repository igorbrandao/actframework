<?php
	
	/**
	 * Configuration - All system settings
	 *
	 * @since 0.1
	*/
	class Configuration 
	{
		/** 
         * Configuration attributes
        */
        public $APP_NAME;
		public $MAX_EXECUTION_TIME;
		public $OS;
		public $ABSPATH;
		public $UP_ABSPATH;
		public $FULL_PERMISSION;
		public $HOME_URI;
		public $ACTIVE_TAB;
		public $DB_NAME;
		public $DB_USER;
		public $DB_PASSWORD;
		public $DB_CHARSET;
		public $DEBUG;
		public $MAX_FILE_SIZE;
		public $HOSTNAME;

		/**
         * Configuration constructor
        */
		public function __construct( $APP_NAME_, $HOME_URI_, $DEBUG_, $ACTIVE_TAB_, $HOSTNAME_, $DB_NAME_, $DB_USER_, $DB_PASSWORD_, $DB_CHARSET_ )
		{
			// PHP CONFIG
			$this->APP_NAME				= $APP_NAME_;
			$this->MAX_EXECUTION_TIME 	= -1;
	    	$this->OS 					= strtoupper(substr(PHP_OS, 0, 3));
		    $this->ABSPATH 				= dirname( __FILE__ );
		    $this->UP_ABSPATH 			= dirname( __FILE__ ) . '/resources/uploads/';
		    $this->FULL_PERMISSION		= 0777;
		    $this->HOME_URI				= $HOME_URI_;
		    $this->DEBUG				= $DEBUG_;
		    $this->MAX_FILE_SIZE		= (1024*10000);
		    
		    // APPLICATION CONFIG
		    $this->ACTIVE_TAB			= $ACTIVE_TAB_;
		    
		    // DATABASE CONFIG
		    $this->HOSTNAME				= $HOSTNAME_;
		    $this->DB_NAME				= $DB_NAME_;
		    $this->DB_USER				= $DB_USER_;
		    $this->DB_PASSWORD			= $DB_PASSWORD_;
		    $this->DB_CHARSET 			= $DB_CHARSET_;
		}
	}
?>