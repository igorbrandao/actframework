<!-- start: TOOLBAR -->
	<!-- The container of the sidebar and content box -->
	<div role="main" id="main" class="container_12 clearfix" style="margin-top: -20px;">

		<!-- The blue toolbar stripe -->
		<section class="toolbar">
			<div class="user">
				<div class="avatar">
					<img style="height: 32px; width: 32px; border: 0;" src="
					<?php 

						// Check if the user has a photo
						if ( isset($this->getUser()->IMAGE) && strcmp($this->getUser()->IMAGE, "") != 0 )
						{
							echo $this->configuration->HOME_URI . "/" . $this->getUser()->IMAGE;
						}
						else
						{
							echo $this->configuration->HOME_URI . "/framework/assets/img/logos/" . $this->configuration->APP_NAME . "/logo.png";
						}

					?>">
					<span>2</span>
				</div>
				<span><?php 

						// Check if the user has a name
						if ( isset($this->getUser()->NAME) && strcmp($this->getUser()->NAME, "")  != 0 )
						{
							if ( isset($this->getUser()->SURNAME) && strcmp($this->getUser()->SURNAME, "")  != 0 )
							{
								echo $this->getUser()->NAME . " " . $this->getUser()->SURNAME;
							}
							else
							{
								echo $this->getUser()->NAME;
							}
						}

					?></span>
				<ul>
					<li><a href="<?php echo join(DIRECTORY_SEPARATOR, array($this->configuration->HOME_URI, 'UserModule/user_profile')); ?>">Perfil do usuário</a></li>
					<li class="line"></li>
					<li><a href="<?php echo $this->configuration->HOME_URI . "?action=logout" ?>">Logout</a></li>
				</ul>
			</div>

			<!-- Google search -->
			<script>
			  (function() {
			    var cx = '008740153531644540099:8_9kpdvvsvw';
			    var gcse = document.createElement('script');
			    gcse.type = 'text/javascript';
			    gcse.async = true;
			    gcse.src = 'https://cse.google.com/cse.js?cx=' + cx;
			    var s = document.getElementsByTagName('script')[0];
			    s.parentNode.insertBefore(gcse, s);
			  })();
			</script>

			<form name="search-form" class="search-form" action="">
				<input type="search" id="q" name="q" placeholder="Buscar no site..." autocomplete="off" class="tooltip" title="Digite uma palavra para buscar no site" data-gravity=s>
			</form>

		</section><!-- End of .toolbar-->
<!-- end: TOOLBAR -->