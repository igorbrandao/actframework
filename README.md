# README #
** Visão Geral**
O objetivo deste projeto é implementar um framework que de suporte para múltiplas instâncias de sistemas extratores de dados
da base do gover dados.gov. Os dados extraídos são relativos ao histório dos acidentes de trabalho no Brasil entre os anos 1994 - 2016.

### MEMBBROS DA EQUIPE ###
* Débora Costa, Igor A. Brandão, José Neto Gameleira

### COMO EXECUTAR O PROGRAMA ###
Basta executar a URL inicial do projeto, o mesmo se encarregará de iniciar todas as aplicações necessárias