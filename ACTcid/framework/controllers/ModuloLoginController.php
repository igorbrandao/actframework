<?php

	/**
     * Import the framework's necessary itens
    */
    include_once 'framework/controllers/Controller.php';

	/**
	 * Modulo_Login - Controller modulo_login
	 *
	*/
	class ModuloLoginController extends Controller
	{
		/** 
		 * Attributes
		*/
		private $user_ID;

		/**
		 * Get's and set's
		*/
		private function setUserID( $user_ID_ )
		{
			$this->user_ID = $user_ID_;
		}

		private function getUserID()
		{
			return $this->user_ID;
		}

		/** Functions section
		 * Load the page "https://exemplo.com/login_module/"
		*/
		public function index( )
		{
			// Page title
			$this->title = 'ACT - Login';

			// Function parameter
			$this->mvc_parameters = ( func_num_args() >= 1 ) ? func_get_arg(0) : array();

			// Set the page's view and breadcrumb
			unset($this->page->views['header']);
			unset($this->page->views['loading_box']);
		    unset($this->page->views['lock_screen']);
			unset($this->page->views['dialogs']);
			unset($this->page->views['message_box']);
			unset($this->page->views['breadcrumb']);
			unset($this->page->views['navbar']);
			unset($this->page->views['menu_logo']);
			unset($this->page->views['toolbar']);
			unset($this->page->views['sidebar']);
			unset($this->page->views['footer']);
			$this->load_model($_SERVER['DOCUMENT_ROOT'] . '/framework/models/user_module/user_module-model.php');

			$this->page->views["view"] = $_SERVER['DOCUMENT_ROOT'] . '/framework/views/login_module/login-view.php';

			// Return the page itself
			return $this->page;
		} // index
	}
?>