<?php

	/**
     * Import the framework's necessary itens
    */
    include_once 'Controller.php';

	/**
	 * Modulo_Usuario - Controller UserModule
	 *
	 * @since 0.1
	*/
	class UserModuleController extends Controller
	{
		/** Functions section
		 * Load the page "https://www.site.com/UserModule/"
		*/
		public function index( )
		{
			// Page title
			$this->title = $this->configuration->APP_NAME . ' - Gerenciar Usuários';

			// Function parameter
			$this->mvc_parameters = ( func_num_args() >= 1 ) ? func_get_arg(0) : array();

			// Load model
			$this->setModel( $this->load_model('user_module/user_module-model') );
			$this->setModel2( $this->load_model('home_module/home_module-model') );

			// Set the page's view and breadcrumb
			$this->page->views["breadcrumb"] = $_SERVER['DOCUMENT_ROOT'] . '/' . $this->configuration->APP_NAME . '/framework/views/user_module/_breadcrumb_manage_user.php';
			$this->page->views["view"] = $_SERVER['DOCUMENT_ROOT'] . '/' . $this->configuration->APP_NAME . '/framework/views/user_module/manage_user-view.php';
			
			//***********************************************************
            //** EVENT HANDLER'S
            //***********************************************************

            // Store the action from $_GET ( insert, login, delete, etc )
            if ( isset( $_REQUEST["action"] ) )
            {
                // Auxiliar variables
                $action = $_REQUEST["action"];

                // Check the action
                switch ( $action )
                {
                    // Update select box content
                    case 'delete':
                    {
                        // Call function from model instance
                        $this->getModel()->delete_user( $_REQUEST["user_ID"] );
                        break;
                    }
                }
            }

			return $this->page;
		} // index

		/** Functions section
		 * Load the page "https://www.site.com/UserModule/user_registration.php"
		*/
		public function userregistration( )
		{
			// Page title
			$this->title = $this->configuration->APP_NAME . ' - Cadatro de usuário';

			// Function parameter
			$this->mvc_parameters = ( func_num_args() >= 1 ) ? func_get_arg(0) : array();

			// Load model
			$this->setModel( $this->load_model('user_module/user_module-model') );
			$this->setModel2( $this->load_model('home_module/home_module-model') );

			// Set the page's view and breadcrumb
			$this->page->views["breadcrumb"] = $_SERVER['DOCUMENT_ROOT'] . '/' . $this->configuration->APP_NAME . '/framework/views/user_module/_breadcrumb_user_registration.php';
			$this->page->views["view"] = $_SERVER['DOCUMENT_ROOT'] . '/' . $this->configuration->APP_NAME . '/framework/views/user_module/user_registration-view.php';

			return $this->page;
		} // userregistration

		/** Functions section
		 * Load the page "https://www.site.com/UserModule/user_edition.php"
		*/
		public function useredition( )
		{
			// Page title
			$this->title = $this->configuration->APP_NAME . ' - Edição de usuário';

			// Function parameter
			$this->mvc_parameters = ( func_num_args() >= 1 ) ? func_get_arg(0) : array();

			// Load model
			$this->setModel( $this->load_model('user_module/user_module-model') );
			$this->setModel2( $this->load_model('home_module/home_module-model') );

			// Set the page's view and breadcrumb
			$this->page->views["breadcrumb"] = $_SERVER['DOCUMENT_ROOT'] . '/' . $this->configuration->APP_NAME . '/framework/views/user_module/_breadcrumb_user_edition.php';
			$this->page->views["view"] = $_SERVER['DOCUMENT_ROOT'] . '/' . $this->configuration->APP_NAME . '/framework/views/user_module/user_edition-view.php';

			return $this->page;
		} // userregistration

		/** Functions section
		 * Load the page "https://www.site.com/UserModule/user_profile.php"
		*/
		public function userprofile( )
		{
			// Page title
			$this->title = $this->configuration->APP_NAME . ' - Perfil de usuário';

			// Function parameter
			$this->mvc_parameters = ( func_num_args() >= 1 ) ? func_get_arg(0) : array();

			// Load model
			$this->setModel( $this->load_model('user_module/user_module-model') );
			$this->setModel2( $this->load_model('home_module/home_module-model') );

			// Set the page's view and breadcrumb
			$this->page->views["breadcrumb"] = $_SERVER['DOCUMENT_ROOT'] . '/' . $this->configuration->APP_NAME . '/framework/views/user_module/_breadcrumb_user_profile.php';
			$this->page->views["view"] = $_SERVER['DOCUMENT_ROOT'] . '/' . $this->configuration->APP_NAME . '/framework/views/user_module/user_profile-view.php';

			return $this->page;
		} // userprofile
	}
?>