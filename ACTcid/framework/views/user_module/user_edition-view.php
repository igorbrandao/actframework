<!-- start: CONTENT -->
<section id="content" class="container_12 clearfix" data-sort=true>

	<?php

		// Check if service ID is valid
		if ( isset($_GET['USR']) && $_GET['USR'] != '' && strpos($_GET['USR'], "**") !== false )
		{
			$user_id = decrypted_url($_GET['USR'] , "**");

			// Load user information
			if ( is_numeric($user_id) )
			{
				$user_info = $this->mvc_controller->getModel()->get_user_info($user_id);
	
				// Edit function
				$this->mvc_controller->getModel()->edit_user($user_info["ID_USUARIO"]);
			}
		}

		// Check if the user really exists in DB
		if ( !isset($_GET['USR']) || !isset($user_info) )
		{
			?><script>alert("Houve um problema com o identificador do usuário. Por favor, tente novamente.");
			window.location.href = "<?php echo $this->configuration->HOME_URI . '/UserModule'; ?>";</script> <?php
			return false;
		}

	?>

	<h1 class="grid_12 margin-top no-margin-top-phone" title="Editar Usuário">Editar Usuário</h1>

	<div class="grid_12">

		<form action="#" role="form" id="wiz" action="" method="POST" class="box wizard manual validate" enctype="multipart/form-data">

			<!--<div class="header">
				<h2><img src="<?php echo HOME_URI;?>/assets/img/icons/packs/fugue/16x16/ui-tab--arrow.png" class="icon">Wizard</h2>
			</div>-->

			<div class="content">

				<ul class="steps">
					<li><a class="current" href="#wiz_pessoal">Informações pessoais</a></li>
					<li><a href="#wiz_contato">Endereço e contato</a></li>
					<li><a href="#wiz_finalizacao">Finalização</a></li>
				</ul>

				<fieldset id="wiz_pessoal">

					<div class="row">
						<label for="ID_TIPO_USUARIO">
							<strong>Tipo de usuário: </strong>
							<small>(Preenchimento obrigatório)</small>
						</label>
						<div>
							<select name="ID_TIPO_USUARIO" id="ID_TIPO_USUARIO" class="required search">
								<option value="">Selecione...</option>
								<?php

									// User's type list
									$list = $this->mvc_controller->getModel()->get_user_type_list($this->getUser()->USER_TYPE);

									foreach ($list as $value)
									{
										if ( $user_info["ID_TIPO_USUARIO"] == $value[0] )
											echo "<option value='" . $value[0] . "' selected>" . $value[1] . "</option>";
										else
											echo "<option value='" . $value[0] . "'>" . $value[1] . "</option>";
									}

								?>
							</select>
						</div>
					</div>

					<div class="row">
						<label for="PRIMEIRO_NOME">
							<strong>Primeiro nome:</strong>
							<small>(Preenchimento obrigatório)</small>
						</label>
						<div>
							<input type="text" class="required" name="PRIMEIRO_NOME" id="PRIMEIRO_NOME" maxlength="30" value="<?php echo iif($user_info["PRIMEIRO_NOME"]); ?>" />
						</div>
					</div>

					<div class="row">
						<label for="SOBRENOME">
							<strong>Sobrenome:</strong>
							<small>(Preenchimento obrigatório)</small>
						</label>
						<div>
							<input type="text" class="required" name="SOBRENOME" id="SOBRENOME" maxlength="30" value="<?php echo iif($user_info["SOBRENOME"]); ?>" />
						</div>
					</div>

					<div class="row">
						<label for="DATA_NASCIMENTO">
							<strong>Data de nascimento:</strong>
							<small>(Preenchimento obrigatório)</small>
						</label>
						<div>
							<input type="date" class="required" name="DATA_NASCIMENTO" id="DATA_NASCIMENTO" value="<?php echo iif($user_info["DATA_NASCIMENTO"]); ?>" />
						</div>
					</div>

					<div class="row">
						<label for="SEXO">
							<strong>Sexo: </strong>
							<small>(Preenchimento obrigatório)</small>
						</label>
						<div>
							<select class="required search" name="SEXO" id="SEXO">
								<option value="">Selecione...</option> 
								<option value="M">Masculino</option> 
								<option value="F">Feminino</option> 
								<?php

									if ( strcmp($user_info["SEXO"], "M") == 0 )
									{
										echo "<option value='M' selected>Masculino</option>";
										echo "<option value='F'>Feminino</option>";
									}
									else
									{
										echo "<option value='M'>Masculino</option>";
										echo "<option value='F' selected>Feminino</option>";
									}

								?>
							</select>
						</div>
					</div>
					
					<div class="row">
						<label for="ID_TIPO_USUARIO">
							<strong>Profissão: </strong>
							<small>(Preenchimento obrigatório)</small>
						</label>
						<div>
							<select name="CBO" id="CBO" class="required search">
								<option value="">Selecione...</option>
								<?php

									// User's CBO list
									$list = $this->mvc_controller->getModel()->get_user_cbo_list();

									foreach ($list as $value)
									{
										if ( $user_info["ID_CBO"] == $value[0] )
											echo "<option value='" . $value[0] . "' selected>" . $value[1] . "</option>";
										else
											echo "<option value='" . $value[0] . "'>" . $value[1] . "</option>";
									}

								?>
							</select>
						</div>
					</div>


				</fieldset>

				<fieldset id="wiz_contato">

					<div class="row">
						<label for="EMAIL">
							<strong>E-mail:</strong>
							<small>(Preenchimento obrigatório)</small>
						</label>
						<div>
							<input type="text" email="true" class="required" name="EMAIL" id="EMAIL" maxlength="100" value="<?php echo iif($user_info["EMAIL"]); ?>" />
						</div>
					</div>

					<div class="row">
						<label for="CEP">
							<strong>CEP:</strong>
							<small>(Preenchimento obrigatório)</small>
						</label>
						<div>
							<input type="text" class="required" name="CEP" id="CEP" value="<?php echo iif($user_info["CEP"]); ?>"/>
						</div>
					</div>

					<div class="row">
						<label for="CIDADE">
							<strong>Cidade:</strong>
							<small>(Preenchimento obrigatório)</small>
						</label>
						<div>
							<input type="text" class="required" name="CIDADE" id="CIDADE" value="<?php echo iif($user_info["CIDADE"]); ?>"/>
						</div>
					</div>

					<div class="row">
						<label for="UF">
							<strong>UF:</strong>
							<small>(Preenchimento obrigatório)</small>
						</label>
						<div>
							<select class="required" name="UF" id="UF" class="search required">
								<?php

									// State's list
									$list = $this->mvc_controller->getModel()->get_state_list();

									foreach ($list as $value)
									{
										if ( $user_info["ID_UF"] == $value[0] )
											echo "<option value='" . $value[0] . "' selected>" . $value[1] . "</option>";
										else
											echo "<option value='" . $value[0] . "'>" . $value[1] . "</option>";
									}

								?>
							</select>
						</div>
					</div>

				</fieldset>

				<fieldset id="wiz_finalizacao">
					<div class="alert note top">
						<span class="icon"></span>
						<strong>Sucesso!</strong> Todas as informações foram corretamente preenchidas.
					</div>
					<p>Clique no botão &quotFinalizar edição&quot; para encerrar a edição.</p>
				</fieldset>

			</div><!-- End of .content -->

			<div class="actions">
				<div class="left">
					<a href="#" class="button grey"><span><img src="<?php echo $this->configuration->HOME_URI;?>/framework/assets/img/icons/packs/fugue/16x16/arrow-180.png" width=16 height=16></span>Passo anterior</a>
				</div>
				<div class="right">
					<a href="#" class="button grey"><span><img src="<?php echo $this->configuration->HOME_URI;?>/framework/assets/img/icons/packs/fugue/16x16/arrow.png" width=16 height=16></span>Próximo passo</a>
					<a href="#" class="button finish"><span><img src="<?php echo $this->configuration->HOME_URI;?>/framework/assets/img/icons/packs/fugue/16x16/arrow.png" width=16 height=16></span>Finalizar edição</a>
				</div>
			</div><!-- End of .actions -->

		</form><!-- End of .box -->
	</div><!-- End of .grid_4 -->

	<script>
		$$.ready(function(){
			$('#wiz').wizard({
				onSubmit: function(){
					//alert('Your Data:\n' + $('form#wiz').serialize());
					return false;
				}
			});

			// Call the function to mask the input fields
			applyCustomMasks();
		});
	</script>
</section><!-- End of #content -->
<!-- end: CONTENT -->