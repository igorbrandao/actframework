<?php

    /**
	 * User - define the user entity
	 *
	 * @since 0.1
	*/
    class User
    {
        /** User properties */
		public $USER_ID     = '', 		//!< User's ID
			   $USER_TYPE   = '',   	//!< User's Type
			   $NAME        = '',       //!< User's Name
			   $SURNAME     = '',       //!< User's Surname
			   $BIRTH       = '',       //!< User's Birth
			   $GENDER      = '',      //!< User's Gender
			   $EMAIL       = '',       //!< User's Email
			   $CEP         = '',       //!< User's CEP
			   $CITY        = '',       //!< User's City
			   $UF          = '',       //!< User's UF
			   $ID_CBO      = '',       //!< User's ID_CBO
			   $CBO         = '';       //!< User's CBO

        /**
         * User constructor
        */
        public function __construct()
        {
        	// TODO
        }
    }

?>