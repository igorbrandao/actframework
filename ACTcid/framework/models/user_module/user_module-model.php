<?php

	class UserModuleModel
	{
		/**
		 * Stores some configuration 
		*/
		private $localConfig;

		/**
		 * Class constructor
		 *
		 * Set the database, controller, parameter and user data.
		 *
		 * @since 0.1
		 * @access public
		 * @param object $db PDO Conexion object
		 * @param object $controller Controller object
		 * @param object $configuration_ Configuration object
		*/
		public function __construct( $db_ = false, $controller_ = null, Configuration $configuration_ )
		{
			// Set DB (PDO)
			$this->db = $db_;

			// Set the controller
			$this->controller = $controller_;

			// Set the main parameters
			$this->mvc_parameters = $this->controller->mvc_parameters;

			// Set the local configs
			$this->localConfig = $configuration_;

			// Set user data
			//$this->userdata = $this->controller->userdata;

			// Define the active tab
			$configuration_->ACTIVE_TAB = "Users";
		}

		/**
		 * Get state list
		 * 
		 * @since 0.1
		 * @access public
		*/
		public function get_state_list() 
		{
			// Select the necessary data from DB
			$query = $this->db->query('SELECT `ID_UF`, `NOME` FROM `UF` WHERE `DATA_FECHA` IS NULL');

			// Check if query worked
			if ( ! $query )
				return array();

			// Return data to view
			return $query->fetchAll();
		} // get_state_list

		/**
		 * Get user type list
		 * 
		 * @since 0.1
		 * @access public
		 * 
		 * @param $USER_TYPE_ => user type [optional]
		*/
		public function get_user_type_list( $USER_TYPE_ = null ) 
		{
			// Select the necessary data from DB
			$sql = "SELECT `ID_TIPO_USUARIO`, `DESCRICAO` FROM `TIPO_USUARIO` 
				WHERE `DATA_FECHA` IS NULL ";

			// Check if the user is an admin
			if ( $USER_TYPE_ != ADMIN )
			{
				// Add another where condition
				$sql .= " AND `ID_TIPO_USUARIO` != 1 ";
			}

			// Execute the query
			$query = $this->db->query($sql);

			// Check if query worked
			if ( ! $query )
				return array();

			// Return data to view
			return $query->fetchAll();
		} // get_user_type_list
		
		/**
		 * Get user type list
		 * 
		 * @since 0.1
		 * @access public
		 * 
		*/
		public function get_user_cbo_list() 
		{
			// Select the necessary data from DB
			$sql = "SELECT `ID_CBO`, `DESCRICAO` FROM `CBO` 
				WHERE `DATA_FECHA` IS NULL ";

			// Execute the query
			$query = $this->db->query($sql);

			// Check if query worked
			if ( ! $query )
				return array();

			// Return data to view
			return $query->fetchAll();
		} // get_user_cbo_list

		/**
		 * Get user list
		 * 
		 * @since 0.1
		 * @access public
		 * 
		 * @param USER_ID_ => user ID [optional]
		*/
		public function get_user_list( $USER_ID_ = null ) 
		{
			// Select the necessary data from DB
			$sql = "SELECT USR.`ID_USUARIO`, USR.`ID_TIPO_USUARIO`, TUSR.`DESCRICAO` AS TP_USUARIO, USR.`PRIMEIRO_NOME`, 
				USR.`SOBRENOME`, USR.`DATA_CADASTRO`, USR.`DATA_FECHA` 
				FROM 
					`USUARIO` AS USR
				INNER JOIN 
					`TIPO_USUARIO` AS TUSR ON TUSR.`ID_TIPO_USUARIO` = USR.`ID_TIPO_USUARIO`
				WHERE 
					USR.`DATA_FECHA` IS NULL ";

			// Check if the user ID was informed
			if ( is_null($USER_ID_) == false )
			{
				// Add another where condition
				$sql .= " AND USR.`ID_USUARIO` = " . $USER_ID_;
			}

			// Execute the query
			$query = $this->db->query($sql);

			// Check if query worked
			if ( $query )
				return $query->fetchAll();
			else
				return 0;
		} // get_user_list

		/**
		 * Get user info
		 * 
		 * @since 0.1
		 * @access public
		*/
		public function get_user_info( $user_ID_ ) 
		{
			// Select the necessary data from DB
			$sql = "SELECT USR.`ID_USUARIO`, USR.`ID_TIPO_USUARIO`, USR.`PRIMEIRO_NOME`, USR.`SOBRENOME`, 
				USR.`DATA_NASCIMENTO`, USR.`SEXO`, USR.`EMAIL`, USR.`SENHA`, USR.`DATA_CADASTRO`,
				USR.`CEP`, USR.`CIDADE`, UF.`ID_UF`, UF.`NOME` AS UF, CBO.`ID_CBO`, CBO.`DESCRICAO` AS CBO
				FROM 
					`USUARIO` as USR 
				LEFT JOIN 
					`UF` AS UF ON UF.`ID_UF` = USR.`UF`
				LEFT JOIN 
					`CBO` AS CBO ON CBO.`ID_CBO` = USR.`CBO`
				WHERE 
					USR.`ID_USUARIO` = " . $user_ID_ . " AND
					USR.`DATA_FECHA` IS NULL";

			$query = $this->db->query($sql);

			// Check if query worked
			if ( ! $query )
				return array();

			// Return data to view
			return $query->fetch();
		} // get_user_info

		/**
		 * Insert users
		 *
		 * @since 0.1
		 * @access public
		*/
		public function insert_user()
		{
			/**
			 * Check if information was sent from web form with a field called insere_empresa.
			*/
			if ( 'POST' != $_SERVER['REQUEST_METHOD'] )
			{
				return;
			}

			// Generate a standard password to the user
			$auxiliary_array = array();

			// ******************************* HASH MODULE *******************************
			// Base-2 logarithm of the iteration count used for password stretching
			$hash_cost_log2 = 8;

			// Do we require the hashes to be portable to older systems (less secure)?
			$hash_portable = FALSE;

			$phpass = new PasswordHash($hash_cost_log2, $hash_portable);
			// ***************************************************************************

			// Generate a new encrypted password
			$password = $phpass->HashPassword("mudar123");

			$auxiliary_array["SENHA"] = $password;
			$auxiliary_array["DATA_CADASTRO"] = date("d-m-Y H:i:s");

			// Concatenate the auxiliary infos with the POST
			$_POST = $auxiliary_array + $_POST;

			/*
			 * 1º step: insert the address
			*/
			$query = $this->db->insert( 'USUARIO', $_POST );

			// Check if the insertion worked
			if ( $query )
			{
				// Redirect (success)
				?><script>window.location.href = "<?php echo join(DIRECTORY_SEPARATOR, array($this->localConfig->HOME_URI, 'UserModule?status=success&event=insert')); ?>";</script> <?php
			}
			else
			{
				// Redirect (error)
				?><script>window.location.href = "<?php echo join(DIRECTORY_SEPARATOR, array($this->localConfig->HOME_URI, 'UserModule?status=error&event=insert')); ?>";</script> <?php
			}

		} // insert_user

		/**
		 * Edit users
		 *
		 * @param $user_ID_ => user ID
		 *
		 * @since 0.1
		 * @access public
		*/
		public function edit_user( $user_ID_ )
		{
			/**
			 * Check if information was sent from web form with a field called insere_empresa.
			*/
			if ( 'POST' != $_SERVER['REQUEST_METHOD'] )
			{
				return;
			}

			/*
			 * 1º step: update user information
			*/
			$query = $this->db->update( 'USUARIO', 'ID_USUARIO', $user_ID_, $_POST);

			// Check if the insertion worked
			if ( $query )
			{
				// Redirect (success)
				?><script>window.location.href = "<?php echo join(DIRECTORY_SEPARATOR, array($this->localConfig->HOME_URI, 'UserModule?status=success&event=edit')); ?>";</script> <?php
			}
			else
			{
				// Redirect (error)
				?><script>window.location.href = "<?php echo join(DIRECTORY_SEPARATOR, array($this->localConfig->HOME_URI, 'UserModule?status=error&event=edit')); ?>";</script> <?php
			}

		} // edit_user

		/**
		 * Delete an specific user
		 * 
		 * @since 0.1
		 * @access public
		 *
		 * @param $user_ID_ => user ID
		*/
		public function delete_user( $user_ID_ )
		{
			// Auxiliar variables
			$arr_data = array();
			$arr_data["DATA_FECHA"] = date("d-m-Y H:i:s");

			// Check the item type
			if ( isset($user_ID_) && $user_ID_ != "" && $user_ID_ != 0 )
			{
				// Disable the user itself
				$query = $this->db->update( 'USUARIO', 'ID_USUARIO', $user_ID_, $arr_data );

				// Cascade (if it's necessary)
			}

			echo "///";

		} // delete_user
	}

?>