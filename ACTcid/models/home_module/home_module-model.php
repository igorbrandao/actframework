<?php

	class HomeModuleModel
	{
		/**
		 * Class constructor
		 *
		 * Set the database, controller, parameter and user data.
		 *
		 * @since 0.1
		 * @access public
		 * @param object $db PDO Conexion object
		 * @param object $controller Controller object
		 * @param object $configuration_ Configuration object
		*/
		public function __construct( $db_ = false, $controller_ = null, Configuration $configuration_ )
		{
			// Set DB (PDO)
			$this->db = $db_;

			// Set the controller
			$this->controller = $controller_;

			// Set the main parameters
			$this->mvc_parameters = $this->controller->mvc_parameters;

			// Set user data
			//$this->userdata = $this->controller->userdata;

			// Define the active tab
			$configuration_->ACTIVE_TAB = "Dashboard";
		}

		/**
		 * Get the site's statistcs
		 * 
		 * @since 0.1
		 * @access public
		*/
		public function get_users_count() 
		{
			// Select the necessary data from DB
			$query = $this->db->query('SELECT COUNT(`ID_USUARIO`) FROM `USUARIO` WHERE `DATA_FECHA` IS NULL');

			// Check if query worked
			if ( ! $query )
				return array();

			// Return data to view
			return $query->fetchColumn(0);
		} // get_users_count

		/**
		 * Get the count of accidents by UF
		 * 
		 * @since 0.1
		 * @access public
		 * 
		 * param $UF_ => could be the UF id or name
		*/
		public function get_accident_count_byUF( $UF_ = null ) 
		{
			// Check if the UF param was informed
			if ( is_null( $UF_ ) )
			{
				// Select accidents related to all UF
				$sql = "SELECT SUM(`QTD_ACIDENTES`) 
				FROM `ACT_UF` 
				WHERE 
					`DATA_FECHA` IS NULL";
			}
			else
			{
				// Check if the UF is an ID or the name itself
				if ( is_numeric( $UF_ ) )
				{
					// Select accidents related to the user UF
					$sql = "SELECT `NOME` FROM `UF` WHERE ID_UF = " . $UF_ . " AND `DATA_FECHA` IS NULL";
	
					// Execute the query
					$query = $this->db->query($sql);
	
					// Check if the query worked
					if ( ! $query )
						return 0;
	
					// Return data
					$UF_ = $query->fetchColumn(0);
				}
	
				// Select the necessary data from DB
				$sql = "SELECT SUM(`QTD_ACIDENTES`) 
					FROM `ACT_UF` 
					WHERE 
						`UF` like '%" . $UF_ . "%' AND 
						`DATA_FECHA` IS NULL";
			}

			// Execute the query
			$query = $this->db->query($sql);

			// Check if query worked
			if ( ! $query )
				return array();

			// Return data to view
			return $query->fetchColumn(0);
		} // get_accident_count_byUF

		/**
		 * Get the count of accidents by gender
		 * 
		 * @since 0.1
		 * @access public
		 * 
		 * param $GENDER_ => user gender
		*/
		public function get_accident_count_byGender( $GENDER_ = null ) 
		{
			// Check if the gender was informed
			if ( is_null( $GENDER_) )
			{
				// Select accidents related to all genders
				$sql = "SELECT SUM(`QTD_ACIDENTES`) 
					FROM `ACT_IDADE` 
					WHERE
						`DATA_FECHA` IS NULL";
			}
			else
			{
				// Select accidents related to the user gender
				$sql = "SELECT SUM(`QTD_ACIDENTES`) 
					FROM `ACT_IDADE` 
					WHERE 
						LEFT(`SEXO` , 1) = '" . substr($GENDER_, 0, 1) . "' AND 
						`DATA_FECHA` IS NULL";
			}

			// Execute the query
			$query = $this->db->query($sql);

			// Check if query worked
			if ( ! $query )
				return array();

			// Return data to view
			return $query->fetchColumn(0);
		} // get_accident_count_byGender
		
		/**
		 * Get the count of accidents by birth month
		 * 
		 * @since 0.1
		 * @access public
		 * 
		 * param $GENDER_ => user gender
		*/
		public function get_accident_count_byBirthMonth( $BIRTH_MONTH_ = null ) 
		{
			// Array with months
			$array_months = array();
			$array_months[1]	= "Janeiro"; 
			$array_months[2]	= "Fevereiro";
			$array_months[3]	= "Março";
			$array_months[4]	= "Abril";
			$array_months[5]	= "Maio";
			$array_months[6]	= "Junho";
			$array_months[7]	= "Julho";
			$array_months[8]	= "Agosto";
			$array_months[9]	= "Setembro";
			$array_months[10]	= "Outubro";
			$array_months[11]	= "Novembro";
			$array_months[12]	= "Dezembro";

			// Check if the birth month was informed
			if ( is_null( $BIRTH_MONTH_ ) )
			{
				// Select accidents related to all months
				$sql = "SELECT SUM(`QTD_ACIDENTES`) 
					FROM `ACT_MES` 
					WHERE
						`DATA_FECHA` IS NULL";
			}
			else
			{
				// Select accidents related to the user birth month
				$sql = "SELECT SUM(`QTD_ACIDENTES`) 
					FROM `ACT_MES` 
					WHERE 
						`MES` like '%" . $array_months[(int)$BIRTH_MONTH_] . "%' AND 
						`DATA_FECHA` IS NULL";
			}

			// Execute the query
			$query = $this->db->query($sql);

			// Check if query worked
			if ( ! $query )
				return array();

			// Return data to view
			return $query->fetchColumn(0);
		} // get_accident_count_byBirthMonth
	}

?>